#include <stdio.h>
#include <stdlib.h>
#include <time.h> //Ajout de la librairie pour le random

//Structure du jeu
struct Jeu
{
    unsigned int combinaison;
    unsigned int nbEssaie;
    unsigned int partie;
    unsigned int choix;
};

//Structure position
struct Position
{
    unsigned int positionOk;
    unsigned int positionNok;
};

//Fonction de comparaison des combinaisons
struct Position controlePlacement (struct Jeu choixJoueur[4], struct Jeu aDeviner[4])
{
    //Initialisation des variables
    int i;
    int j;
    struct Position positionNok;
    struct Position positionOk;

    for (i=0; i<4; i++)
    {
        //On test si les deux combinaisons sont identiques
        if (choixJoueur[i].choix == aDeviner[i].combinaison)
        {
            //On renvoit combien de chiffre sont bien plac�s
            return positionOk;
        }
        else
        {
            for (j=0; j<4; j++)
            {
                //On test si 1 chiffre du tableau du joueur est pr�sent dans le tableau g�n�r� automatiquement
                if (choixJoueur[i].choix != aDeviner[j].combinaison)
                {
                    //On renvoit combien de chiffre sont mal bien plac�s
                    return positionNok;
                }
            }
        }
    }
}

int main()
{
    //Initialisation du random
    srand(time(NULL));

    //D�claration des variables
    unsigned int rejouer = 1;
    struct Jeu aDeviner[4];
    struct Jeu nbEssaieRestant;
    struct Jeu choixJoueur[4];
    struct Position positionOk;
    struct Position positionNok;
    struct Jeu partieGagne;
    struct Jeu partiePerdue;
    int i;

    printf("*** Bienvenue dans le jeu Mastermind ***\n");

    //Boucle du jeu
    while (rejouer)
    {
        //Generation de la combinaison
        for (i=0; i<4; i++)
        {
            aDeviner[i].combinaison=rand()%10;
        }

        //Initialisation du nombre d'essaie � 10
        nbEssaieRestant.nbEssaie = 10;

        while(nbEssaieRestant.nbEssaie != 0 && (choixJoueur[i].choix != aDeviner[i].combinaison))
        {
            //on demande au joueur son choix de combinaison
            for(i=0 ; i<4 ; i++)
            {
                printf("\nVeuillez saisir votre combinaison (entre 0 et 9)\n Position %d --> ",i+1);
                //Permet de placer le "pointeur" � la fin du buffer
                fseek(stdin, 0, SEEK_END);
                scanf("%u",&choixJoueur[i].choix);
            }

            //on test via une fonction les chiffres bien plac�s
            controlePlacement(choixJoueur, aDeviner);
            printf("\n*** Comparaison avec le MASTER ***\n");

            //Si c'est identique le joueur gagne
                //On incremente la variable partieGagne.Jeu



            //Sinon on lui affiche lesquels sont bien plac�s et on lui retire un essai
            nbEssaieRestant.nbEssaie--;
            printf("\n --- Il nous vous reste plus que %d ---\n", nbEssaieRestant.nbEssaie);


            //Il perd on lui affiche la bonne solution et on incremente la variable partiePerdu.Jeu

        }

        //On demande au joueur si il veut rejouer ou non
        printf("\nVoulez-vous rejouer ? (1:Rejouer, 0:Quitter)");
        //Permet de placer le "pointeur" � la fin du buffer
        fseek(stdin, 0, SEEK_END);
        scanf("%u",&rejouer);
    }
    return 0;
}
